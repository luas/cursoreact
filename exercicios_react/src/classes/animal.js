export default class Animal {
    constructor (nome) {
        this.nome = nome
    }
    toString(){
        return `Animal: ${this.nome}`
    }
    setNome(novoNome){
        this.nome = novoNome
    }
}