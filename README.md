CursoReact
=================================
CANAL DO YOUTUBE

https://www.youtube.com/cod3rcursos


REPOSITÓRIO DO CURSO

Todos os exercícios e projetos estão em... https://github.com/cod3rcursos/curso-react-redux

APOSTILA DO CURSO

http://files.cod3r.com.br/apostila-react-redux.pdf


Ultima aula:
https://www.udemy.com/react-redux-pt/learn/v4/t/lecture/10477212?start=0

Ambiente linux:
===============================
Instalar NodeJS:
	Instalar conforme : https://github.com/nodesource/distributions/blob/master/README.md
	a versao desejada.
Instalar mongodb:
	https://www.mongodb.com/download-center/community
	Instalar o .deb
	no home: sudo mkdir /data
			 sudo mkdir /data/db
			 sudo chmod 777 -R /data/
	Ver se mongodb esta instalado com:
		netstat -l e ver se a porta 27017 esta ouvindo.
IDE:
	Instalar webstorm: 
	https://www.jetbrains.com/webstorm/
	
Webpack:
=========
	 Dentro da pasta:
	 npm init -y
	 npm i --save-dev webpack webpack-dev-server
	 npm i --save-dev webpack-cli
	 npm i --save-dev babel-core
	 npm i --save-dev babel-loader@7
	 npm i --save-dev babel-loader
	 npm i --save-dev babel-preset-es2015
	 npm i --salve-dev babel-plugin-transform-object-rest-spread (recurso clonar objeto)
	 npm i --save-dev react (dependencia do react)
	 npm i --save-dev babel-preset-react (dependencia babel X react)
Css dependencias: 
=========
	npm i --save-dev css-loader mini-css-extract-plugin style-loader
React dependencias:
========
	npm i --save-dev react react-dom babel-core babel-loader babel-loader@7 babel-preset-es2015
	npm i --save-dev babel-preset-react webpack webpack-dev-server webpack-cli
	obs: babel-core 8 precisa do babel-core 7
	
Imagem dependencias:
===================
	npm i --save-dev file-loader url-loader 
	npm i --save-dev babel-plugin-transform-react-jsx-img-import
	tutorial: 	https://survivejs.com/webpack/loading/images/
				https://blog.hellojs.org/importing-images-in-react-c76f0dfcb552
				https://medium.com/a-beginners-guide-for-webpack-2/handling-images-e1a2a2c28f8d

Deploy Heruku
=================
sudo snap install --classic heroku

App novo npm:
============
	Ver log heroku:
		heroku logs -a AppName --tail
	https://blog.heroku.com/deploying-react-with-zero-configuration

	Inside that directory, you can run several commands:

	  npm start
		Starts the development server.

	  npm run build
		Bundles the app into static files for production.

	  npm test
		Starts the test runner.

	  npm run eject
		Removes this tool and copies build dependencies, configuration files
		and scripts into the app directory. If you do this, you can’t go back!
